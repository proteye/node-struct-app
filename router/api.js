'use strict';

const express = require('express');
const co = require('co');
const _ = require('lodash');
const moment = require('moment');

let router = express.Router();

let config = require(__basedir +'/config');
let logger = require(__basedir +'/lib/logger');
let fn = require(__basedir +'/lib/fn');

// GET: http://domain.ltd/api/info
router.get('/info', function(req, res) {
    res.json([
	{
	    code: "dev",
	    name: "Development"
	}, {
	    code: "pro",
	    name: "Production"
	}
    ]);
});

module.exports = router;